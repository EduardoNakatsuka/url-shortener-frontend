import axios from 'axios';
import { useCallback, useState } from 'react';

type VerbTypes = 'get' | 'post';

export type Link = {
  id: number;
  full_url: string;
  short_url: string;
  views: number;
};

const useApi = () => {
  const API_URL = process.env.REACT_APP_BACKEND_URL;
  const [loading, setLoading] = useState<boolean>(false);
  const [apiError, setApiError] = useState<string>('');

  const resource = useCallback(async (verb: VerbTypes, url: string, params: any = null) => {
    url = `${API_URL}/${url}`;
    setApiError('');
    setLoading(true);
    try {
      const { data } = await axios[verb](url, params);
      return data;
    } catch (error: any) {
      setApiError(error?.response?.data?.message || 'Some error occurred');
    } finally {
      setLoading(false);
    }
  }, [setApiError, setLoading, API_URL]);

  const getResource = useCallback(async (url: string, params: any = null) => {
    return await resource('get', url, params);
  }, [resource]);
  const postResource = useCallback(async (url: string, params: any) => {
    return await resource('post', url, params);
  }, [resource]);

  const createLink = useCallback(async (url: string): Promise<Link> => {
    return await postResource('create', {
      fullUrl: url,
    });
  }, [postResource]);

  const getTop100 = useCallback(async (): Promise<Link[]> => {
    return await getResource('');
  }, [getResource]);

  return {
    loading,
    apiError,
    createLink,
    getTop100,
  };
};

export default useApi;
