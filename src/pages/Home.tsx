import React, { useCallback, useEffect, useState } from 'react';
import useApi from '../hooks';
import { Link } from '../hooks/useApi';

export const Home: React.FC = () => {
  const [url, setUrl] = useState<string>('');
  const [link, setLink] = useState<Link|undefined>(undefined);
  const { loading, apiError, createLink, getTop100 } = useApi();
  const [top100, setTop100] = useState<Link[]|undefined>(undefined);

  const handleSubmit = useCallback(async (event: React.FormEvent) => {
    event.preventDefault();
    if (loading) return;

    const createdLink = await createLink(url);
    if (createdLink) {
      return setLink(createdLink)
    }
    setLink(undefined);
  }, [loading, createLink, setLink, url]);

  const generateShortUrl = useCallback((url: string) => {
    return new URL(url, process.env.REACT_APP_BACKEND_URL).href;
  }, []);

  const getRank = useCallback(async () => { 
    setTop100(await getTop100());
  }, [getTop100]);

  useEffect(() => {
    getRank();
  }, [getRank]);

  return (
    <>
      <h1>Short URL</h1>

      <div>
        <form onSubmit={handleSubmit}>
          <label htmlFor="url">URL to be shorted: </label>

          <input
            type="url"
            name="url"
            placeholder="https://example.com"
            pattern="https://.*"
            required
            onChange={event => setUrl(event.target.value)}
            disabled={loading}
          />

          <button disabled={loading}>
            Short URL
          </button>
        </form>

        {link
          ? (
            <p>Link was created:
              <a href={link.short_url}>
                {link.short_url}
              </a>
            </p>
          ) : ''
        }

        {apiError
          ? (
            <p className="error-paragraph">Error: <b>{apiError}</b></p>
          ) : ''
        }
      </div>

      {top100?.length
        ? (
          <div className="top-100">
            Most Viewed Pages:
            <ul>
              {top100?.map(link => (
                <li key={link.id}>
                  <a href={generateShortUrl(link.short_url)}>
                    {link.full_url}
                  </a> viewed {link.views} times.
                </li>
              ))}
            </ul>
          </div>
        ): ''
      }
    </>
  );
};