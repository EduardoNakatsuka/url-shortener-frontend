# URL SHORTENER

[![NodeJS](https://img.shields.io/badge/node.js-LTS-6DA55F?&logo=node.js&logoColor=white&labelColor=green&color=black)](https://nodejs.org/en)
[![MySql](https://img.shields.io/badge/MySql-5.7-E74430.svg?&logo=MySql&logoColor=white&labelColor=blue&color=important)](https://www.oracle.com/mysql)
[![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?&logo=typescript&logoColor=white)](https://www.typescriptlang.org)
[![ExpressJS](https://img.shields.io/badge/express.js-%23404d59.svg?&logo=express&logoColor=%2361DAFB&labelColor=black)](https://expressjs.com)
[![ReactJS](https://img.shields.io/badge/react-%2320232a.svg?logo=react&logoColor=%2361DAFB)](https://reactjs.org)
[![Yarn](https://img.shields.io/badge/yarn-%232C8EBB.svg?logo=yarn&logoColor=white)](https://reactjs.org)


URL shortener developed by [Eduardo Nakatsuka](https://www.linkedin.com/in/eduardo-nakatsuka)

# Getting started
  - [Requirements](#requirements)
  - [Instalation](#instalation)
  - [Routes](#routes)
  - [Improvements](#improvements)
  - [Considerations](#considerations)

# Requirements

- NodeJS 14.17.6 LTS
https://nodejs.org/en/

- Yarn 1.22.5
https://classic.yarnpkg.com/en/docs/install#debian-stable

- NPX
https://www.npmjs.com/package/npx

# Instalation

1. Install frontend dependencies
```
yarn install
```

2. Copy .env.example to .env
```
cp .env.example
```

3. Run local server
```
yarn start
```

4. (If needed) - Build for production
```
yarn build
```

# Routes

Currently there are 3 routes:
  - GET `/`: Returns top 100 most viewed URLs.
  - POST `/create`: Creates a short URL.
  - GET `/:shortUrl`: Redirects short URL to target URL.

## Improvements

  1. Add tests to both backend and frontend.
  2. Add load test to backend.
  3. Treat errors in a better / more readable way.
  4. Improve show Top 100 logics. **WARNING** - [More on considerations](#considerations).
  5. Improve collision number - [More on considerations](#considerations).
  6. Add .eslint file.
  7. Study with the team a better flow for duplicate URL (redirect user to already created URL? Allow duplication?)
  8. Develop a better UI/UX.
  9. Add CORS for production.

## Considerations

This was really fun to code. It is a very simple project, and in order to keep it under 4-5 hours of both frontend and backend, I tried going as simple as possible, thus working with ExpressJS and Knex, which are very simple and fun to use.


As stated in [Improvements](#Improvements) the top 100 logics is very basic and **IS NOT SCALABLE**. A better approach would be caching with [Redis](https://redis.io/), incrementing each view counter on Redis, and creating a cron job to batch update the database.
I would love doing that, but in order to keep things under 4 hours and without no specifications/requirements I decided to keep it as a TODO. URLs are shortened to 10 characters, which would handle 100 URLs being created per hour, taking ~174 years to basically have a 1% probability of at least one colission according to [NanoID](https://zelark.github.io/nano-id-cc/). It was decided to also index unique the short URL field in the table (Which makes it more non scalable, but enforcing collisions don't happen on database).

Other improvements forementioned could be solved with a few more days, more explanation on the size of the application, and depending on the real need for it.

Thanks for the opportunity!
